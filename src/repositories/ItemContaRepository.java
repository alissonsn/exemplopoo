/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import java.util.ArrayList;
import java.util.List;
import model.ItemConta;

/**
 *
 * @author Alisson Nascimento
 */
public class ItemContaRepository {
    
    private final List<ItemConta> itensConta;

    public ItemContaRepository() {
        itensConta = new ArrayList<>();
    }
    
    public void addItemConta(ItemConta ItemConta){ 
        itensConta.add(ItemConta);
    }
    
    public void removeItemConta(ItemConta ItemConta) {
        itensConta.remove(ItemConta);
    }
      
    public List<ItemConta> listar(){
        return itensConta;
    }
}
