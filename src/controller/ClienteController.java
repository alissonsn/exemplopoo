/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Cliente;
import repositories.ClienteRepository;

/**
 *
 * @author itamir.filho
 */
public class ClienteController {
    
    private ClienteRepository clienteRepository;
    
        
    public ClienteController(ClienteRepository ClienteRepository) {
        this.clienteRepository = ClienteRepository;
    }

    public void salvar(Cliente Cliente) {
        clienteRepository.addCliente(Cliente);
    }
    
    public void remover(Cliente Cliente) {
        clienteRepository.removeCliente(Cliente);
    }
    
    public void atualizar(Cliente ClienteOld, Cliente ClienteNew){
        clienteRepository.removeCliente(ClienteOld);
        clienteRepository.addCliente(ClienteNew);
    }
    
    public String listar() {
        String lista = "Lista de clientes \n";
        for (Cliente Cliente : clienteRepository.listar()) {
            lista += Cliente + "\n";
        }
        return lista;
    }
}
