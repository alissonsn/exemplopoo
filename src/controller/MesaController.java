/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Mesa;
import repositories.MesaRepository;

/**
 *
 * @author itamir.filho
 */
public class MesaController {
    
    private MesaRepository mesaRepository;
    
        
    public MesaController(MesaRepository MesaRepository) {
        this.mesaRepository = MesaRepository;
    }

    public void salvar(Mesa Mesa) {
        mesaRepository.addMesa(Mesa);
    }
    
    public void remover(Mesa Mesa) {
        mesaRepository.removeMesa(Mesa);
    }
    
    public void atualizar(Mesa MesaOld, Mesa MesaNew){
        mesaRepository.removeMesa(MesaOld);
        mesaRepository.addMesa(MesaNew);
    }
    
    public String listar() {
        String lista = "Lista de Mesas \n";
        for (Mesa Mesa : mesaRepository.listar()) {
            lista += Mesa + "\n";
        }
        return lista;
    }
}
