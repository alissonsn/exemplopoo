/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Conta;
import repositories.ContaRepository;

/**
 *
 * @author Alisson Nascimento
 */
public class ContaController {
    
    private ContaRepository contaRepository;
    
        
    public ContaController(ContaRepository ContaRepository) {
        this.contaRepository = ContaRepository;
    }

    public void salvar(Conta Conta) {
        contaRepository.addConta(Conta);
    }
    
    public void remover(Conta Conta) {
        contaRepository.removeConta(Conta);
    }
    
    public void atualizar(Conta ContaOld, Conta ContaNew){
        contaRepository.removeConta(ContaOld);
        contaRepository.addConta(ContaNew);
    }
    
    public String listar() {
        String lista = "Lista de contas \n";
        for (Conta Conta : contaRepository.listar()) {
            lista += Conta + "\n";
        }
        return lista;
    }
}
