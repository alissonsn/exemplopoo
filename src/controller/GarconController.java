/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Garcon;
import repositories.GarconRepository;

/**
 *
 * @author Alisson Nascimento
 */
public class GarconController {
    
    private GarconRepository garconRepository;
    
        
    public GarconController(GarconRepository GarconRepository) {
        this.garconRepository = GarconRepository;
    }

    public void salvar(Garcon Garcon) {
        garconRepository.addGarcon(Garcon);
    }
    
    public void remover(Garcon Garcon) {
        garconRepository.removeGarcon(Garcon);
    }
    
    public void atualizar(Garcon GarconOld, Garcon GarconNew){
        garconRepository.removeGarcon(GarconOld);
        garconRepository.addGarcon(GarconNew);
    }
    
    public String listar() {
        String lista = "Lista de garçons \n";
        for (Garcon Garcon : garconRepository.listar()) {
            lista += Garcon + "\n";
        }
        return lista;
    }
}
