/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.ItemConta;
import repositories.ItemContaRepository;

/**
 *
 * @author itamir.filho
 */
public class ItemContaController {
    
    private ItemContaRepository itemContaRepository;
    
        
    public ItemContaController(ItemContaRepository ItemContaRepository) {
        this.itemContaRepository = ItemContaRepository;
    }

    public void salvar(ItemConta ItemConta) {
        itemContaRepository.addItemConta(ItemConta);
    }
    
    public void remover(ItemConta ItemConta) {
        itemContaRepository.removeItemConta(ItemConta);
    }
    
    public void atualizar(ItemConta ItemContaOld, ItemConta ItemContaNew){
        itemContaRepository.removeItemConta(ItemContaOld);
        itemContaRepository.addItemConta(ItemContaNew);
    }
    
    public String listar() {
        String lista = "Lista de ItemConta\n";
        for (ItemConta ItemConta : itemContaRepository.listar()) {
            lista += ItemConta + "\n";
        }
        return lista;
    }
}
