/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.GarconController;
import javax.swing.JOptionPane;
import model.Garcon;
import repositories.GarconRepository;

/**
 *
 * @author Alisson Nascimento
 */
public class GarconView {
    
    private GarconController garconController;
    
    private GarconRepository garconRepository;
    
    public GarconView() {
        garconRepository = new GarconRepository();
        garconController = new GarconController(garconRepository);
    }

    public GarconController getGarconController() {
        return garconController;
    }

    public void setGarconController(GarconController GarconController) {
        this.garconController = GarconController;
    }

    public GarconRepository getGarconRepository() {
        return garconRepository;
    }

    public void setGarconRepository(GarconRepository GarconRepository) {
        this.garconRepository = GarconRepository;
    }
    
    public static void main(String args[]) {
        String nome = JOptionPane.showInputDialog("Nome:");
        String cpf = JOptionPane.showInputDialog("CPF:");
        String matricula = JOptionPane.showInputDialog("Matrícula:");
        
        Garcon garcon = new Garcon();
        garcon.setNome(nome);
        garcon.setCpf(cpf);
        garcon.setMatricula(matricula);
        
        GarconView GarconView = new GarconView();
        GarconView.getGarconController().salvar(garcon);
        
        JOptionPane.showMessageDialog(null, 
                GarconView.getGarconController().listar());
    }
    
}
