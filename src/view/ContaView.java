/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.ContaController;
import javax.swing.JOptionPane;
import model.Conta;
import repositories.ContaRepository;

/**
 *
 * @author Alisson Nascimento
 */
public class ContaView {
    
    private ContaController contaController;
    
    private ContaRepository contaRepository;
    
    public ContaView() {
        contaRepository = new ContaRepository();
        contaController = new ContaController(contaRepository);
    }

    public ContaController getContaController() {
        return contaController;
    }

    public void setContaController(ContaController ContaController) {
        this.contaController = ContaController;
    }

    public ContaRepository getContaRepository() {
        return contaRepository;
    }

    public void setContaRepository(ContaRepository ContaRepository) {
        this.contaRepository = ContaRepository;
    }
    
    public static void main(String args[]) {
        
    }
    
}
