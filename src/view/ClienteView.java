/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.ClienteController;
import javax.swing.JOptionPane;
import model.Cliente;
import repositories.ClienteRepository;

/**
 *
 * @author Alisson Nascimento
 */
public class ClienteView {
    
    private ClienteController clienteController;
    
    private ClienteRepository clienteRepository;
    
    public ClienteView() {
        clienteRepository = new ClienteRepository();
        clienteController = new ClienteController(clienteRepository);
    }

    public ClienteController getClienteController() {
        return clienteController;
    }

    public void setClienteController(ClienteController ClienteController) {
        this.clienteController = ClienteController;
    }

    public ClienteRepository getClienteRepository() {
        return clienteRepository;
    }

    public void setClienteRepository(ClienteRepository ClienteRepository) {
        this.clienteRepository = ClienteRepository;
    }
    
    public static void main(String args[]) {
        String nome = JOptionPane.showInputDialog("Nome:");
        String cpf = JOptionPane.showInputDialog("CPF:");
        
        Cliente cliente = new Cliente();
        cliente.setNome(nome);
        cliente.setCpf(cpf);
        
        ClienteView clienteView = new ClienteView();
        clienteView.getClienteController().salvar(cliente);
        
        JOptionPane.showMessageDialog(null, 
                clienteView.getClienteController().listar());
    }
    
}
