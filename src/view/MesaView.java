/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.MesaController;
import repositories.MesaRepository;

/**
 *
 * @author Alisson Nascimento
 */
public class MesaView {
    
    private MesaController mesaController;
    
    private MesaRepository mesaRepository;
    
    public MesaView() {
        mesaRepository = new MesaRepository();
        mesaController = new MesaController(mesaRepository);
    }

    public MesaController getMesaController() {
        return mesaController;
    }

    public void setMesaController(MesaController MesaController) {
        this.mesaController = MesaController;
    }

    public MesaRepository getMesaRepository() {
        return mesaRepository;
    }

    public void setMesaRepository(MesaRepository MesaRepository) {
        this.mesaRepository = MesaRepository;
    }
    
    public static void main(String args[]) {
        
    }
    
}
