/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.ItemContaController;
import repositories.ItemContaRepository;

/**
 *
 * @author Alisson Nascimento
 */
public class ItemContaView {
    
    private ItemContaController itemContaController;
    
    private ItemContaRepository itemContaRepository;
    
    public ItemContaView() {
        itemContaRepository = new ItemContaRepository();
        itemContaController = new ItemContaController(itemContaRepository);
    }

    public ItemContaController getItemContaController() {
        return itemContaController;
    }

    public void setItemContaController(ItemContaController ItemContaController) {
        this.itemContaController = ItemContaController;
    }

    public ItemContaRepository getItemContaRepository() {
        return itemContaRepository;
    }

    public void setItemContaRepository(ItemContaRepository ItemContaRepository) {
        this.itemContaRepository = ItemContaRepository;
    }
    
    public static void main(String args[]) {
        
    }
    
}
